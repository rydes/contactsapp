import { NgModule } from 'angular-ts-decorators';
import 'angular-material'
import 'angular-animate';
import 'angular-aria';
import { routes } from './app.routes';
import { ContactsModule } from './components/contacts.module';

export interface IComponentState extends ng.ui.IState {
  state: string;
  component?: any;
  views?: { [name: string]: IComponentState };
}

@NgModule({
  name: 'AppModule',
  imports: [
    'ui.router',
    'ngSanitize',
    'ngMaterial',
    'ngAria',
    'ngAnimate',
    ContactsModule
  ],
  declarations:[]
})
export class AppModule {

  private static toKebabCase = (string) => {
    return string.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
  };

  private static setTemplate(state: IComponentState) {
    var selector = Reflect.getMetadata("custom:name", state.component);
    state.template = `<${this.toKebabCase(selector)}></${this.toKebabCase(selector)}>`;
    delete state.component;
  }

  private static provideStates(states: IComponentState[], $stateProvider: ng.ui.IStateProvider) {
    states.map((config) => {
      const name = config.state;
      const namedState = config.views;
      if (namedState) {
        const namedViews = Object.keys(namedState);
        namedViews.forEach((view) => {
          AppModule.setTemplate(namedState[view]);
        });
      }
      else {
        AppModule.setTemplate(config);
      }
      delete config.state;
      return { name, config };
    }).forEach(state => {
        console.log(state);
      $stateProvider.state(state.name, state.config);
    });
  }

  /*@ngInject*/
  config($urlRouterProvider: ng.ui.IUrlRouterProvider,
    $stateProvider: ng.ui.IStateProvider) {
    AppModule.provideStates(routes, $stateProvider);
    $urlRouterProvider.otherwise('/');
  }

  /*@ngInject*/
  run($window: ng.IWindowService, $q: ng.IQService) {
    $window.Promise = $q;
  }
}
