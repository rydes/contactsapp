import Contactsservice = require("../../contacts.service");
import ContactsService1 = Contactsservice.ContactsService;

export class ContactsDynamicList {

  constructor(private ContactsService: ContactsService1) {
  }

  loadedItems = [];
  numItems = 10;
  PAGE_SIZE = 10;
  searchQuery;
  numLoaded_: 0;

  toLoad_: 0;
  refresh = function () {
    this.numLoaded_ = 0;
    this.toLoad_ = 0;
    this.loadedItems = [];
  }

  search = function (query) {
    this.searchQuery = query;
    this.refresh();
  }

  fetchMoreItems_ = function (index) {
    var pageNumber = Math.floor(index / this.PAGE_SIZE);
    if (this.toLoad_ < index) {
      this.toLoad_ += 10;

      let getContactsPromise;
      if (this.searchQuery)
        getContactsPromise = this.ContactsService.getContacts(pageNumber * this.PAGE_SIZE, 10, this.searchQuery)
      else
        getContactsPromise = this.ContactsService.getContacts(pageNumber * this.PAGE_SIZE, 10)

      getContactsPromise.then(response => {
        this.numLoaded_ = this.toLoad_;
        this.numItems = response.count;
        response.contacts.forEach(x => this.loadedItems.push(x));
      });
    }
  }

  getItemAtIndex = function (index) {
    if (index > this.numLoaded_) {
      this.fetchMoreItems_(index);
      return null;
    }
    return this.loadedItems[index];
  }

  getLength = function () {
    return this.numItems;
  };
}
