import { Component, Input } from 'angular-ts-decorators';
import { ContactsService, IContact } from '../../contacts.service'
import './contacts-list.scss';

import Contactsdynamiclist = require("./contacts-dynamic-list");
import ContactsDynamicList = Contactsdynamiclist.ContactsDynamicList;

const template = require('./contacts-list.html');

@Component({
  selector: 'contactsList',
  template
})
export class ContactsListComponent implements ng.IComponentController {

  scrollableListItem: ContactsDynamicList;
  search: string;

  /*@ngInject*/
  constructor(private ContactsService: ContactsService,
    private $scope: angular.IScope,
    private $window: angular.IWindowService,
    private $state: angular.ui.IStateService) {
    this.scrollableListItem = new ContactsDynamicList(ContactsService);
  }

  $onInit() {
    this.adjustContactsListHeight();

    this.$scope.$watch(() => this.search, (newVal) => {
      this.scrollableListItem.search(newVal);
    })
  }

  goToContactDetails(contact) {
    this.$state.go('contact-details', { contactId: contact._id });
  }

  adjustContactsListHeight() {
    this.$window.addEventListener('resize', onResize);
    function onResize() {
      this.$scope.$digest();
    }
  }

  getListHeight(){
    return { height: '' + (this.$window.innerHeight - 190) + 'px' };
  };

}

