import { NgModule } from 'angular-ts-decorators';
import { ContactsListComponent } from './contacts-list/contacts-list.component';
import { AddContactComponent } from './add-contact/add-contact.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { ContactsService } from '../contacts.service';

@NgModule({
  name: 'ContactsModule',
  declarations: [
    ContactsListComponent,
    AddContactComponent,
    ContactDetailsComponent
  ],
  providers: [
    ContactsService
  ]
})
export class ContactsModule {}
