import { Component } from 'angular-ts-decorators';
import { ContactsService, AddContact } from '../../contacts.service'

const template = require('./add-contact.html');

@Component({
  selector: "addContact",
  template: template
})
export class AddContactComponent implements ng.IComponentController {

  contact: AddContact;
  isSubmitting = false;

  /*@ngInject*/
  constructor(private ContactsService: ContactsService,
              private $state: angular.ui.IStateService) {
  }

  addContact(form: angular.IFormController) {
    if (form.$invalid)
      return;

    this.isSubmitting = true;
    this.ContactsService.addContact(this.contact)
      .then(() => this.$state.go('contacts-list'))
      .catch(() => {
        this.isSubmitting = false;
      });
  }
}

