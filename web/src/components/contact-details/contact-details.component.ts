import { ContactsService, IContact } from '../../contacts.service'
import { Component } from 'angular-ts-decorators';

const template = require('./contact-details.html');

@Component({
  selector: "contactDetails",
  template: template
})
export class ContactDetailsComponent implements ng.IComponentController {
  contactId: string;
  contact: IContact;

  /*@ngInject*/
  constructor(private ContactsService: ContactsService,
    private $stateParams) {
    this.contactId = $stateParams.contactId;
  }

  $onInit() {
    this.ContactsService
      .getContactById(this.contactId)
      .then(response => this.contact = response);
  }
}

