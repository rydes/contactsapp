import { IComponentState } from './main';
import { AddContactComponent } from './components/add-contact/add-contact.component';
import { ContactsListComponent } from './components/contacts-list/contacts-list.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';


export const routes: IComponentState[] = [
  { state: 'add-contact', url: '/add-contact', component: AddContactComponent },
  { state: 'contacts-list', url: '/', component: ContactsListComponent },
  { state: 'contact-details', url: '/contact-details/:contactId', component: ContactDetailsComponent }
];
