import { Injectable } from 'angular-ts-decorators';

export interface IContact {
  _id: string,
  name: string,
  number: string,
  otherInfo: string
}
export type GetContactList = {
  skip: number,
  take: number,
  nameOrNumberContains?: string
}

export type GetContactListResponse = {
  contacts: IContact[],
  count: number
}

export type AddContact = {
  name: string,
  number: string,
  otherInfo: string
}

export type AddContactResponse = {
  _id: string;
}


@Injectable('ContactsService')
export class ContactsService {


  /*@ngInject*/
  constructor(private $http: angular.IHttpService) {
  }

  public getContacts(skip: number, take: number, nameOrNumberContains?: string): angular.IPromise<GetContactListResponse> {
    let request: GetContactList = {
      skip: skip,
      take: take,
      nameOrNumberContains: nameOrNumberContains
    };

    return this.$http.get<GetContactListResponse>("api/contacts", { params: request }).then(response => {
      return response.data;
    });
  }

  public addContact(contact: AddContact):angular.IPromise<AddContactResponse> {
    return this.$http.post<AddContactResponse>("api/contacts", contact).then(response => {
      return response.data;
    });
  }
  public getContactById(contactId): angular.IPromise<IContact> {
    return this.$http.get<IContact>(`api/contacts/${contactId}`).then(response => {
      return response.data;
    });
  }
}
