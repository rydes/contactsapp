from flask import Flask, Blueprint
from flask import jsonify
from flask import request
from bson.objectid import ObjectId
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from marshmallow import Schema, fields, post_load
from api import app_config
from api.infrastructure import database
from api.infrastructure import routes
from api.request_handlers import *

app_config.loadConfig()
database.setUpConnection()

app = Flask(__name__)
app.register_blueprint(routes.api, url_prefix = '/api')

@app.route('/')
def basic_pages(**kwargs):
    return make_response(open('static/index.html').read())


if __name__ == '__main__':
    app.run(debug=False)