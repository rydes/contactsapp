from bson.objectid import ObjectId
import pymongo
from ..infrastructure.database import getDb
import re

def get_query_filter(get_contacts_request):
    if get_contacts_request.nameOrNumberContains is None:
        return None
    escaped_query_string = re.escape(get_contacts_request.nameOrNumberContains) 
    query_string = {'$regex': f'.*{escaped_query_string}.*', '$options': 'i'} 
    return { '$or': [{"name":query_string}, {"number": query_string }]}


def get_contacts(fiter, skip, take):

    return list(getDb().contacts.find(fiter).skip(skip).limit(take).sort('name', pymongo.ASCENDING))

def get_contacts_count(filter):
    return getDb().contacts.find(filter).count()

def get_contact_by_id(id):
  return getDb().contacts.find_one({'_id': ObjectId(id)})