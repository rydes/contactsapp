from os import path
import json

config = {}
  
def loadConfig():
    config_path = path.relpath("./api/api.config.json")
    with open(config_path) as json_data_file:
        data = json.load(json_data_file)
    global config 
    config = data
