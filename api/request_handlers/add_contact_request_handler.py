from flask import abort, jsonify
from webargs.flaskparser import use_args, use_kwargs
from marshmallow import Schema, fields, post_load
from marshmallow.validate import  Length
from ..infrastructure import routes
from ..commands import contacts_commands

class AddContactRequestSchema(Schema):
    def __init__(self, strict=True, **kwargs):
        super(Schema, self).__init__(strict=strict, **kwargs)

    name = fields.Str(required = True, validate=Length(min=1))
    number = fields.Str(required = True, validate=Length(min=1))
    otherInfo = fields.Str()

    @post_load()
    def make_contact(self, data):
        return AddConctacRequest(**data)

class AddConctacRequest:
    def __init__(self, name, number, otherInfo = None):
        self.name= name
        self.number = number
        self.otherInfo = otherInfo

request_schema = AddContactRequestSchema()
@routes.api.route('/contacts', methods=['POST'])
@use_args(request_schema)
def add_contact(new_contact):
  data, errors = request_schema.dump(new_contact);

  if(errors):
    return abort(400)

  contact_id = contacts_commands.add_contact(data)

  return jsonify({'id' : str(contact_id)})
