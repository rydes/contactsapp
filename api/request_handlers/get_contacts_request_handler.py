from marshmallow import Schema, fields, post_load
from flask import jsonify
from webargs.flaskparser import use_args, use_kwargs
from .common_schemas import ContactSchema
from ..queries import contacts_queries
from ..infrastructure import routes


class GetContactsRequest:
    def __init__(self, skip, take, nameOrNumberContains = None):
        self.skip = skip
        self.take = take
        self.nameOrNumberContains = nameOrNumberContains

class GetContactsResponse:
    def __init__(self, contacts, count):
        self.contacts = contacts
        self.count = count

class GetContactsResponseSchema(Schema):
    def __init__(self, strict=True, **kwargs):
        super(Schema, self).__init__(strict=strict, **kwargs)

    contacts = fields.List(fields.Nested(ContactSchema))
    count = fields.Int()
    
    @post_load
    def make_response(self, data):
        return GetContactsResponse(**data)


class GetContactsRequestSchema(Schema):
    skip = fields.Int(required = True)
    take = fields.Int(required = True)
    nameOrNumberContains = fields.Str(default = None)
    
    @post_load
    def make_request(self, data):
        return GetContactsRequest(**data)

contact_schema = ContactSchema()
get_contacts_request_schema = GetContactsRequestSchema()
get_contacts_response_schema = GetContactsResponseSchema()


def create_response(contacts, count):
    mapped_contacts = list(map(lambda contact: contact_schema.load(contact).data, contacts))
    response = GetContactsResponse(mapped_contacts, count)
    serializable_response = get_contacts_response_schema.dump(response).data
    return serializable_response  
    
@routes.api.route('/contacts', methods=['GET'])
@use_args(get_contacts_request_schema)
def get_contacts_handler(get_contacts_request):
    filter = contacts_queries.get_query_filter(get_contacts_request)
    contacts = contacts_queries.get_contacts(filter, get_contacts_request.skip,get_contacts_request.take)
    count = contacts_queries.get_contacts_count(filter)
    response = create_response(contacts, count)
    return jsonify(response)

 