from flask import abort, jsonify
from bson import ObjectId
from ..infrastructure import routes
from ..queries import contacts_queries
from .common_schemas import ContactSchema


@routes.api.route('/contacts/<string:id>', methods=['GET'])
def get_contact_by_id(id):
  if not ObjectId.is_valid(id):
    abort(404)

  document = contacts_queries.get_contact_by_id(id)

  if (document is None):
    abort(404)

  contact_schema = ContactSchema()
  contact = contact_schema.load(document).data

  return jsonify(contact_schema.dump(contact).data)


