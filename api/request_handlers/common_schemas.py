from marshmallow import Schema, fields, post_load
from .common_models import Contact
from bson.objectid import ObjectId

class ObjectIdField(fields.Field):

    def __init__(self, *args, **kwargs):
        super(ObjectIdField, self).__init__(*args, **kwargs)

    default_error_messages = {
        'invalid_ObjectId': 'Not a valid ObjectId string.'
    }

    def _deserialize(self, value, attr, data):
        if value is None:
            return None
        return str(value)


class ContactSchema(Schema):
    _id= ObjectIdField()
    name= fields.Str(required=True)
    number= fields.Str(required=True)
    otherInfo= fields.Str(required=True)

    @post_load
    def make_contact(self, data):
        return Contact(**data)


