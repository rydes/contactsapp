from pymongo import MongoClient
from .. import app_config

def setUpConnection():
    global connection 
    connection= MongoClient(app_config.config['mongo_connection_string'])

def getDb():
    return connection.db