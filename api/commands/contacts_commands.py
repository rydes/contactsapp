from ..infrastructure.database import getDb

def add_contact(contact):
    return getDb().contacts.insert_one(contact).inserted_id