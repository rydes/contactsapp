import unittest
from bson.objectid import ObjectId
from ..request_handlers.common_models import Contact
from ..request_handlers.common_schemas import ContactSchema
from ..request_handlers.add_contact_request_handler import AddContactRequestSchema
from ..request_handlers.get_contacts_request_handler import GetContactsRequestSchema, GetContactsResponseSchema, GetContactsResponse

class SchemaMappingTests(unittest.TestCase):

    def test_can_map_mongo_document_to_contact(self):
        #arrange
        document = {
            '_id': ObjectId(),
            'name': 'John Kowalski',
            'number': '+48666222333',
            'otherInfo': 'My client'
            }

        contact_schema = ContactSchema()
        
        #act
        mapped = contact_schema.load(document).data

        #assert
        self.assertEqual(mapped._id, str(document['_id']))
        self.assertEqual(mapped.name, document['name'])
        self.assertEqual(mapped.number, document['number'])
        self.assertEqual(mapped.otherInfo, document['otherInfo'])

    def test_can_map_create_contact_request_args(self):
        #arrange
        request = {
            'name': 'John Kowalski',
            'number': '+48666222333',
            'otherInfo': 'My client'
            }

        new_contact_schema = AddContactRequestSchema()
        
        #act
        mapped = new_contact_schema.load(request).data

        #assert
        self.assertEqual(mapped.name, request['name'])
        self.assertEqual(mapped.number, request['number'])
        self.assertEqual(mapped.otherInfo, request['otherInfo'])

    def test_can_map_get_contacts_request_args(self):
        #arrange
        request = {
            'skip': '10',
            'take': '20',
            'nameOrNumberContains': 'John'
            }

        get_contacts_request_schema = GetContactsRequestSchema()
        
        #act
        mapped = get_contacts_request_schema.load(request).data

        #assert
        self.assertEqual(mapped.skip, int(request['skip']))
        self.assertEqual(mapped.take, int(request['take']))
        self.assertEqual(mapped.nameOrNumberContains, request['nameOrNumberContains'])

    def test_can_map_get_contacts_request_args(self):
        #arrange
        request = {
            'skip': '10',
            'take': '20',
            'nameOrNumberContains': 'John'
            }

        get_contacts_request_schema = GetContactsRequestSchema()
        
        #act
        mapped = get_contacts_request_schema.load(request).data

        #assert
        self.assertEqual(mapped.skip, int(request['skip']))
        self.assertEqual(mapped.take, int(request['take']))
        self.assertEqual(mapped.nameOrNumberContains, request['nameOrNumberContains'])

    def test_can_serialize_get_contacts_response(self):
        #arrange
        contacts = [Contact('1','John Kowalski','123 321 123', 'Dentist'),  Contact('2','Andrew Nowak', '123 123', 'IT guy')]
        expected_output = [{'_id': '1', 'name': "John Kowalski", 'number': '123 321 123', 'otherInfo': 'Dentist'}, {'_id': '2', 'name': 'Andrew Nowak', 'number': '123 123', 'otherInfo':'IT guy'}]
        total_count = 10
        get_contacts_response = GetContactsResponse(contacts, total_count)

        #act
        get_contacts_response_schema = GetContactsResponseSchema()
        serialized = get_contacts_response_schema.dump(get_contacts_response).data
        serialized_contacts = serialized['contacts']
        #assert
        self.assertEqual(serialized['count'], 10)
        self.assertDictEqual(expected_output[0] , serialized_contacts[0])
        self.assertDictEqual(expected_output[1] , serialized_contacts[1])



if __name__ == '__main__':
    unittest.main()