from os import path
import unittest
import unittest.mock
from bson.objectid import ObjectId
from unittest.mock import patch
import time
import datetime
import json
from pymongo import MongoClient

class ContactQueriesFixture(ContactQueriesFixture):
    @classmethod
    def setUpClass(cls):
        cls.persistTestData()

    @classmethod
    def persistTestData(cls):
        cls.persistedContacts = [{
        '_id': ObjectId(),
        'name': 'Carl Kowalski',
        'number': '333 333 333',
        'otherInfo': 'Carpenter'
        },
        {
        '_id': ObjectId(),
        'name': 'Barnie Kowalski',
        'number': '222 222 222',
        'otherInfo': 'Barista'
        },
        {'_id': ObjectId(),
        'name': 'Andrew Kowalski',
        'number': '111 1111 111',
        'otherInfo': 'Actor'
        }]

        for document in cls.persistedContacts:
            cls.test_db.contacts.insert_one(document)
    

