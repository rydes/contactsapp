from os import path
import unittest
import unittest.mock
from bson.objectid import ObjectId
from unittest.mock import patch
import time
import json
from pymongo import MongoClient

class MongoDbTestFixture(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.setupTestDb()
        cls.persistTestData()
   
    @classmethod
    def setupTestDb(cls):
        config_path =  path.relpath("./api/tests/config.json")
        with open(config_path) as json_data_file:
            data = json.load(json_data_file)

        test_db_conennction_string = data['test_db_conennction_string']

        cls.test_db_name =  str(time.time()).replace('.','')

        cls.mongo_client = MongoClient(test_db_conennction_string)
        cls.test_db = cls.mongo_client[cls.test_db_name]

    @classmethod
    def persistTestData(cls):
        cls.persistedContacts = [{
        '_id': ObjectId(),
        'name': 'Carl Kowalski',
        'number': '333 333 333',
        'otherInfo': 'Carpenter'
        },
        {
        '_id': ObjectId(),
        'name': 'Barnie Kowalski',
        'number': '222 222 222',
        'otherInfo': 'Barista'
        },
        {'_id': ObjectId(),
        'name': 'Andrew Kowalski',
        'number': '111 1111 111',
        'otherInfo': 'Actor'
        }]

        for document in cls.persistedContacts:
            cls.test_db.contacts.insert_one(document)
    
    @classmethod
    def tearDownClass(cls):
        pass
        #cls.mongo_client.drop_database(cls.test_db_name)

