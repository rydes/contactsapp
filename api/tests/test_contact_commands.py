import json
from os import path
import unittest
import unittest.mock
from bson.objectid import ObjectId
from unittest.mock import patch
import time
import datetime
from pymongo import MongoClient
from .mongodb_test_fixture import MongoDbTestFixture
from ..commands import contacts_commands

class ContactCommandsTests(MongoDbTestFixture):

    @patch(contacts_commands.__name__+'.getDb')
    def test_can_get_contact_by_id(self, getDbMock):
        ##arrange
        getDbMock.return_value = self.test_db
        contact_to_add = {
        '_id': ObjectId(),
        'name': 'Carl Kowalski',
        'number': '333 333 333',
        'otherInfo': 'Carpenter'
        }

        #act
        contacts_commands.add_contact(contact_to_add)
        added_contact = self.test_db.contacts.find_one({'_id': contact_to_add['_id']})

        #assert
        assert added_contact == contact_to_add

