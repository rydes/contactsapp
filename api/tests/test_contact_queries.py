import json
from os import path
import unittest
import unittest.mock
from bson.objectid import ObjectId
from unittest.mock import patch
import time
import datetime
from pymongo import MongoClient
from .mongodb_test_fixture import MongoDbTestFixture
from ..queries import contacts_queries
from ..request_handlers.get_contacts_request_handler import GetContactsRequest

class ContactQueriesTests(MongoDbTestFixture):

    def test_can_get_query_filter_when_no_search_string_provided(self):
        #arrange
        request = GetContactsRequest(1,10, None)

        #act
        filter = contacts_queries.get_query_filter(request)

        #assert
        self.assertEqual(filter, None)

    @patch(contacts_queries.__name__+'.getDb')
    def test_can_get_contact_by_id(self, getDbMock):
        ##arrange
        getDbMock.return_value = self.test_db

        #act
        contact = contacts_queries.get_contact_by_id(self.persistedContacts[0]['_id'])

        #assert
        assert contact == self.persistedContacts[0]

    @patch(contacts_queries.__name__+'.getDb')
    def test_can_get_sorted_contacts_list_without_filter(self, getDbMock):
        ##arrange
        getDbMock.return_value = self.test_db

        #act
        contacts = contacts_queries.get_contacts(None, 0, 3)

        #assert
        assert contacts[0] == self.persistedContacts[2]
        assert contacts[1] == self.persistedContacts[1]
        assert contacts[2] == self.persistedContacts[0]
   
    @patch(contacts_queries.__name__+'.getDb')
    def test_can_filter_contacts_by_name(self, getDbMock):
        ##arrange
        getDbMock.return_value = self.test_db
        request = GetContactsRequest(0,10, "Andrew")
        filter = contacts_queries.get_query_filter(request)

        #act
        contacts = contacts_queries.get_contacts(filter, 0, 3)

        #assert
        assert len(contacts) == 1
        assert contacts[0]['name'] == "Andrew Kowalski"

    @patch(contacts_queries.__name__+'.getDb')
    def can_get_contacts_count_with_filter(self, getDbMock):
        ##arrange
        getDbMock.return_value = self.test_db
        request = GetContactsRequest(0,10, "Andrew")
        filter = contacts_queries.get_query_filter(request)

        #act
        contacts_count = contacts_queries.get_contacts_count(filter)

        #assert
        assert len(contacts_count) == 1

    @patch(contacts_queries.__name__+'.getDb')
    def can_get_contacts_count_without_filter(self, getDbMock):
        ##arrange
        getDbMock.return_value = self.test_db

        #act
        contacts_count = contacts_queries.get_contacts_count(None)

        #assert
        assert len(contacts_count) == 3